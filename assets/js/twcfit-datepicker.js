jQuery(function() {
	jQuery(".date-picker").daterangepicker({
		datepickerOptions: {
			 minDate: null,
			 maxDate: null
		},
		altFormat: "mm/dd/yy"
	});
});
