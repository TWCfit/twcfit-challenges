jQuery(function($){

	// find the values we want to change
	$('select.change-values').on('change', function(e){

		action = $(this).data('next-action');
		the_form = $(this).closest('form');
		the_nonce = $(the_form).find('input[name=_wpnonce]').val();

		$('.the-settings-fields').hide();

		var data = {
			'action':	'twcfit-points-editor',
			'subaction':	action,
			'_nonce':	the_nonce,
			'values':	$('select', the_form).serialize(),
		};

		$.post( twcfit.ajaxurl, data, function( response ){

			if( response.success ) {

				switch ( response.data.target) {
					case 'points-fields':

						$('#n-points', the_form ).val( response.data.message[0].n_points );
						$('#f-points', the_form ).val( response.data.message[0].f_points );
						$('#w-points', the_form ).val( response.data.message[0].w_points );
						$('#free-day', the_form ).prop( 'checked', ( "1" == response.data.message[0]['free_day'] ) );

						$('.the-settings-fields', the_form ).show();

						break;

					case 'add-points-fields':
						$( '.the-settings-fields', the_form ).show();
						if( 1 == response.data.message ) {
							$( '.freeday-not-taken', the_form ).find('input').replaceWith( '<p>Free Day Already Taken</p>' );
						}
						break;

					default:
						$( response.data.target, the_form ).html( response.data.message );
						$( response.data.target, the_form ).closest( '.settings-field' ).show();

				}
			}

		}, 'json' );

	} );

	/* Prevent form submission via Enter key on Points Editor
	 * http://stackoverflow.com/questions/585396/how-to-prevent-enter-keypress-to-submit-a-web-form
	 */
	function checkEnter(e){
		 e = e || event;
		 var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
	 return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
	}
	var selection = document.querySelector('.manage-points form') !== null;
	if ( selection ) {
	  	document.querySelector('.manage-points form').onkeypress = checkEnter;
	}


	// Update the Points table on button click
	$('form input[type=submit]', '.manage-points' ).on( 'click', function(e){
		submit_form( $(e.target).prop('name'), e );
		return false;
	} );

	submit_form = function( subaction, e ){

		the_form = $(e).closest('form');

		if( $('#free-day', the_form).prop('checked') ){
			freeday = 1;
		} else {
			freeday = 0;
		}

		var data = {
			'action':		'twcfit-points-editor',
			'subaction':	subaction,
			'_nonce':		$(the_form).find('input[name=_wpnonce]').val(),
			'data':		{
				'keys': $('select', the_form ).serialize(),
				'values':	$('input', the_form ).serialize(),
				'free-day':	freeday
			}

		};

		$.post( twcfit.ajaxurl, data, function( response ){

			if( response.success ) {
console.log(response);
			}

		}, 'json' );

	};

	// Add New Points table on button click
	$('form input[type=submit]', '.add-points' ).on( 'click', function(e){

		submit_form( $(e.target).prop('name'), e );
		return false;
	} );

	submit_form = function( subaction, e ){

		the_form = $(e.target).closest('form');

		if( $('#free-day', the_form ).prop('checked') ){
			freeday = 1;
		} else {
			freeday = 0;
		}

		var data = {
			'action':		'twcfit-points-editor',
			'subaction':	subaction,
			'_nonce':		$(the_form).find('input[name=_wpnonce]').val(),
			'data':		{
				'keys': $('select', the_form ).serialize(),
				'values':	$('input', the_form ).serialize(),
				'free-day':	freeday
			}

		};

		$.post( twcfit.ajaxurl, data, function( response ){

			if( response.success ) {
console.log(response);
			}

		}, 'json' );

	};

});
