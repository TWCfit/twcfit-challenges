jQuery(function($){

	$.validate({
	  form : '#record-challenge-points',
	  modules: 'html5',
	  validateOnBlur: false,
	  errorMessagePosition: $('#add-points-error-message'),
	  scrollToTopOnError: true,
	  onSuccess: function(){

		var data = {
				'action':	'add-points',
				'points':	$('#record-challenge-points').serialize(),
			};
		$.post( twcfit.ajaxurl, data, function( response ){

			if( response.success ) {
				$('#item-body').html( response.data.message );
			}
		}, 'json' );

	  	return false;
	  }
	});

});
