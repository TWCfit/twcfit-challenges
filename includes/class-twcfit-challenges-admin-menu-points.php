<?php

class TWCfit_Challenges_Admin_Menu_Points {

	private static function get_challenges_as_options(){

		$challenges = twcfit_points()->get_unique_challenges();
		$options = array( "Select Challenge&hellip;" => 0 );

		if( $challenges ) :
			foreach( $challenges as $challenge ){

				$group = groups_get_group( array( 'group_id' => $challenge->challenge_id ) );

				$options[ $group->name ] = $group->id;

			}
		endif;

		foreach( $options as $prompt => $value ):

			printf('<option value="%d" >%s</option>', $value, esc_html( $prompt ) );

		endforeach;
	}

	public static function points_editor(){

		$points_limits = apply_filters( 'twcfit_challenge_points_max', array( 'n' => 12, 'f' => 3, 'w' => 2 ) );
		$nonce = wp_create_nonce( 'points-editor' );
?>
<style>
 .manage-points { display: table; table-layout: fixed; }
 .settings-field { display: table-row; width: 50%; padding-bottom: 2rem; }
 label { display: table-cell; }
 select, input { display: table-cell; }
 .points-management > div { float: left; padding: 0 1em; }
</style>
		<div class="wrap">
			<h2>Manage TWC Challenge Points</h2>
			<div class="points-management clearfix">
				<div class="points-management-editor"><?php self::edit_points_form($points_limits, $nonce ); ?></div>
				<div class="points-management-add"><?php self::add_points_form($points_limits, $nonce ); ?></div>
			</div>
		</div>
<?php
	}

	private static function edit_points_form( $points_limits, $nonce ){
?>
			<h3>Edit Points</h3>
			<p>Use the controls to find a set of points that you want to change</p>
			<div class="manage-points">

				<form>
					<input type="hidden" name="_wpnonce" value="<?php echo $nonce; ?>" />
					<div class="settings-field">
					<label for="challenge-id">Which Challenge?</label>
					<select name="challenge-id" class="change-values" data-next-action="update-user-ids">
						<?php self::get_challenges_as_options(); ?>
					</select>
					</div>

					<div class="settings-field" style="display: none;" >
					<label for="user-id">Which User?</label>
					<select id="user-id" name="user-id" class="change-values" data-next-action="update-activity-dates">
						<option value="">Select User&hellip;</option>
					</select>
					</div>

					<div class="settings-field" style="display: none;" >
					<label for="activity-date">Which Date?</label>
					<select id="activity-date" name="activity-date" class="change-values" data-next-action="get-points-values">
						<option value="">Select Activity Date&hellip;</option>
					</select>
					</div>

					<div class="the-settings-fields" style="display: none;" >
						<div class="settings-field">
							<label for="n-points">Nutrition Points</label>
							<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['n']; ?> value=0 id="n-points" name="n-points" />
						</div>

						<div class="settings-field">
							<label for="f-points">Fitness Points</label>
							<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['f']; ?> value=0 id="f-points" name="f-points" />
						</div>

						<div class="settings-field">
							<label for="w-points">Wellness Points</label>
							<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['w']; ?> value=0 id="w-points" name="w-points" />
						</div>

						<div class="settings-field">
							<label for="free-day">Take as Free Day?</label>
							<input type="checkbox" value=0 id="free-day" name="free-day" />
						</div>

						<input type="submit" class="button" id="delete-points" name="delete-points" value="Delete Points" />
						<input type="submit" class="button-primary" id="update-points" name="update-points" value="Update Points" />

					</div>

				</form>
			</div>
<?php
	}

	private static function add_points_form($points_limits, $nonce){
?>
		<h3>Add Points</h3>
			<p>Administrator can add points for members for any date (today or in the past)
			<div class="add-points">

				<form>
					<input type="hidden" name="_wpnonce" value="<?php echo $nonce; ?>"/>
					<div class="settings-field">
					<label for="challenge-id">Which Challenge?</label>
					<select name="challenge-id" class="change-values" data-next-action="get-member-ids">
						<?php self::get_challenges_as_options(); ?>
					</select>
					</div>

					<div class="settings-field" style="display: none;" >
					<label for="user-id">Which User?</label>
					<select id="user-id" name="user-id" class="change-values" data-next-action="which-dates">
						<option value="">Select User&hellip;</option>
					</select>
					</div>

					<div class="settings-field" style="display: none;" >
						<label for="activity-date">Which Date?</label>
						<select id="activity-date" name="activity-date" class="change-values" data-next-action="add-points-values">
							<option value="">Select Activity Date&hellip;</option>
						</select>
					</div>

					<div class="the-settings-fields" style="display: none;" >
						<div class="settings-field">
							<label for="n-points">Nutrition Points</label>
							<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['n']; ?> value=0 id="n-points" name="n-points" />
						</div>

						<div class="settings-field">
							<label for="f-points">Fitness Points</label>
							<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['f']; ?> value=0 id="f-points" name="f-points" />
						</div>

						<div class="settings-field">
							<label for="w-points">Wellness Points</label>
							<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['w']; ?> value=0 id="w-points" name="w-points" />
						</div>

						<div class="settings-field freeday-not-taken">
							<label for="free-day">Take as Free Day?</label>
							<input type="checkbox" value=0 id="free-day" name="free-day" />
						</div>

						<input type="submit" class="button-primary" id="admin-add-points" name="admin-add-points" value="Add Points" />

					</div>

				</form>
			</div>
<?php
	}
}