<?php

class TWCfit_Challenges_Admin_Points {

	function __construct(){

		check_admin_referer('points-editor', '_nonce');

		$args = array();
		if( isset( $_POST ) && isset( $_POST['values'] ) ) {
			parse_str( $_POST['values'], $args );
		}
		switch ( $_POST['subaction'] )
		{
		    case "update-user-ids":
				$this->update_user_ids( (int) $args['challenge-id'] );
		        break;

		    case 'get-member-ids':
		    	$this->get_challenge_members( (int) $args['challenge-id'] );
		    	break;

		    case "update-activity-dates":
				$this->update_activity_dates( (int) $args['challenge-id'], (int) $args['user-id'] );
		    	break;

			case "which-dates":
				$this->check_for_previous_entry( (int) $args['challenge-id'], (int) $args['user-id'] );
				break;

		    case "get-points-values":
				$this->get_points_values( (int) $args['challenge-id'], (int) $args['user-id'], $args['activity-date'] );
		    	break;

		    case 'add-points-values';
		    	$this->show_add_points_form( (int) $args['challenge-id'], (int) $args['user-id'], $args['activity-date'] );
				break;

		    case "update-points":
		    	$this->update_points();
		    	break;

		    case "delete-points":
		    	$this->delete_points();

		    case 'admin-add-points':
		    	$this->add_points();
		    	break;

		    default:

		        break;
		}

	}

	function update_user_ids( $challenge_id ){

		$users = twcfit_points()->get_unique_users_by_challenge( $challenge_id );

		if( $users ):

			$output = '<option value="">Select User&hellip;</option>';
			foreach( $users as $user ){
				$output .= sprintf( '<option value="%d">%s</option>', $user->user_id, esc_html( twc_member_name( $user->user_id ) ) );
			}

			wp_die( json_encode( array(
				'success' => true,
				'data' => array(
					'target'	=> '#user-id',
					'message'	=> $output,
				),
			) ) );

		endif;
	}

	private function get_challenge_members( $challenge_id) {

		$members = $this->get_challenge_member_list( $challenge_id );

		if( $members ):

			$output = '<option value="">Select User&hellip;</option>';
			foreach( $members as $user_handle => $user_id ){
				$output .= sprintf( '<option value="%d">%s</option>', $user_id, esc_html( twc_member_name( $user_id ) ) );
			}

			wp_die( json_encode( array(
				'success' => true,
				'data' => array(
					'target'	=> '#user-id',
					'message'	=> $output,
				),
			) ) );

		endif;
	}

	private function get_challenge_member_list( $group_id ){

		$memberlist = array();
		if ( bp_group_has_members( array('group_id' => $group_id, 'per_page' => 99999, 'page' => 1 ) ) ) :

			while( bp_group_members() ) : bp_group_the_member();

				$display_name = strtolower( bp_get_profile_field_data( array( 'field' => 1, 'user_id' => bp_get_member_user_id() ) ) );
				$memberlist[ $display_name ] = bp_get_member_user_id();

			endwhile;

		endif;

		ksort( $memberlist );

		return $memberlist;
	}

	function update_activity_dates( $challenge_id, $user_id ){

		$points = twcfit_points()->get_points( $challenge_id, $user_id );

		if( $points ) :
			$output = '<option value="">Select Activity Date&hellip;</option>';
			foreach( $points as $recorded_points ){
				$output .= sprintf( '<option value="%1$s">%1$s</option>', $recorded_points->activity_date );
			}

			wp_die( json_encode( array(
				'success' => true,
				'data' => array(
					'target'	=> '#activity-date',
					'message'	=> $output,
				),
			) ) );

		endif;
	}

	private function check_for_previous_entry( $challenge_id, $user_id ){

		$points = twcfit_points()->get_points( $challenge_id, $user_id );
		$have_dates	= wp_list_pluck( $points, 'activity_date');

		$dates = TWCfit_Challenges_BuddyPress::get_challenge_dates( $challenge_id );

		$begin = new DateTime( $dates['start'], twcfit()->tz );
		$end = min( new DateTime( $dates['end'], twcfit()->tz ), new DateTime( 'now', twcfit()->tz ) );
//		$end = $end->modify( '+1 day' );

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod( $begin, $interval ,$end );

		$no_dates = array();
		$output = '<option value="">Select Activity Date&hellip;</option>';
		foreach( $daterange as $date ){
			if( in_array ( $date->format( 'Y-m-d' ), $have_dates ) ) continue; // already have this date
			$output .= sprintf( '<option value="%1$s">%1$s</option>', $date->format( 'Y-m-d') );
		}

		wp_die( json_encode( array(
			'success' => true,
			'data' => array(
				'target'	=> '#activity-date',
				'message'	=> $output,
			),
		) ) );

	}

	function get_points_values( $challenge_id, $user_id, $activity_date ){

		$points = twcfit_points()->get_points( $challenge_id, $user_id, $activity_date );
		if( $points ){
			wp_die( json_encode( array(
				'success' => true,
				'data' => array(
					'target'	=> 'points-fields',
					'message'	=> $points,
				),
			) ) );
		}
	}

	function show_add_points_form( $challenge_id, $user_id, $activity_date ){
		$taken_freeday = twcfit_points()->get_free_day( $challenge_id, $user_id );
		wp_die( json_encode( array(
			'success' => true,
			'data' => array(
				'target'	=> 'add-points-fields',
				'message'	=> ( $taken_freeday ? 1 : 0 ),
			),
		) ) );
	}

	function update_points(){

		$keys = array();
		parse_str( $_POST['data']['keys'], $keys );

		$values = array();
		parse_str( $_POST['data']['values'], $values );

		$entry_date = new DateTime( $keys['activity-date'], twcfit()->tz() );

		$where = array(
			'user_id'	=> (int) $keys['user-id'],
			'challenge_id'	=> (int) $keys['challenge-id'],
			'activity_date' => $entry_date->format( 'Y-m-d' ),
		);

		$updates = array(
			'free_day'		=> ( (int) $values['free-day'] ) ? 1 : 0,
		);
		foreach( array( 'n-points' => 'n', 'f-points' => 'f', 'w-points' => 'w') as $key => $point ) :
			if( isset( $values[$key] ) ) $updates[ $point . '_points' ] = (int) $values[$key];
		endforeach;

		twcfit_points()->update_points( $updates, $where );

		wp_die( json_encode( array(
			'success' => true,
			'data' => array(
				'message' => sprintf( '<h3>You have successfully recorded your points for %s</h3>', $entry_date->format( 'F jS' ) ),
			),
		) ) );

	}

	function delete_points(){

		$keys = array();
		parse_str( $_POST['data']['keys'], $keys );

		$entry_date = new DateTime( $keys['activity-date'], twcfit()->tz() );

		$where = array(
			'user_id'	=> (int) $keys['user-id'],
			'challenge_id'	=> (int) $keys['challenge-id'],
			'activity_date' => $entry_date->format( 'Y-m-d' ),
		);

		twcfit_points()->delete_points( $where );

		wp_die( json_encode( array(
			'success' => true,
			'data' => array(
				'message' => sprintf( '<h3>You have successfully deleted the points for %s</h3>', $entry_date->format( 'F jS' ) ),
			),
		) ) );

	}

	function add_points(){

		$keys = array();
		parse_str( $_POST['data']['keys'], $keys );

		$values = array();
		parse_str( $_POST['data']['values'], $values );

		$update = array(
			'user_id'	=> (int) $keys['user-id'],
			'challenge_id'	=> (int) $keys['challenge-id'],
			'activity_date' => $keys['activity-date'],
			'free_day'		=> $_REQUEST['data']['free-day'],
		);

		foreach( array( 'n-points' => 'n', 'f-points' => 'f', 'w-points' => 'w') as $key => $point ) :
			if( isset($values[$key]) ) $update[ $point . '_points' ] = (int) $values[$key];
		endforeach;

		twcfit_points()->add_points( $update );
	}
}
