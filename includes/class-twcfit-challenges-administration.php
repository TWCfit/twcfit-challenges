<?php

class TWCfit_Challenges_Administration {

	function __construct(){

		wp_enqueue_script( 'form-validate', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js' );
		wp_enqueue_script( 'twcfit-points-admin', plugins_url( '../assets/js/twcfit-points-admin.js', __FILE__ ), array( 'jquery', 'form-validate' ) );
		wp_localize_script( 'twcfit-points-admin', 'twcfit', array(
			'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
		) );

		wp_enqueue_style( 'form-validate', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/theme-default.min.css' );
	}

}