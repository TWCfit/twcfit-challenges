<?php

if( class_exists( 'BP_Group_Extension' ) ) :
class TWCfit_Challenges_BP_Points extends BP_Group_Extension {

	const POSTING_WINDOW_CLOSES = '19:59:00';
	const POSTING_WINDOW_OPENS = '20:00:00';

	public function __construct() {

		$args = array(
			'slug' 			=> 'points',
			'name' 			=> 'Log Points',
			'visibility'	=> 'access',
			'enable_nav_item'	=> 'hide_tab',
		);

		parent::init( $args );

		add_action( 'wp_ajax_add-points', array( $this, 'register_points' ) );

	}

	public static function enqueue_scripts(){

		wp_enqueue_script( 'form-validate', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js' );
		wp_enqueue_script( 'twc-points', plugins_url( '../assets/js/twcfit-points.js', __FILE__ ), array('jquery', 'form-validate') );

		wp_enqueue_style( 'form-validate', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/theme-default.min.css' );

		wp_localize_script( 'twc-points', 'twcfit', array(
			'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
			'add_points'	=> 'add-points',
		) );
	}

	public function display( $group_id = null ){

		$group_id = bp_get_group_id();

		$points_limits = apply_filters( 'twcfit_challenge_points_max', array( 'n' => 12, 'f' => 3, 'w' => 2 ) );

		$now = new DateTimeImmutable( null, twcfit()->tz() ); // now
		$start_time = explode( ':', apply_filters( 'twcfit_challenge_blackout_start', '20:00:00' ) );
		$today_window_start = $now->setTime( $start_time[0], $start_time[1], $start_time[2] ); // Today at 8:00pm

		$end_time = explode( ':', apply_filters( 'twcfit_challenge_blackout_end', '19:59:59' ) );
		$today_window_end = $now->setTime( $end_time[0], $end_time[1], $end_time[2] ); // Today at 7:59pm

  		$dates = groups_get_groupmeta( bp_get_group_id(), '_twcfit_challenge_dates' );

  		if( ! current_user_can( 'administrator' ) ):

			// ignore all the rules if current user is administrator
			if( is_array( $dates ) ) {

				$challenge_starts = new DateTime( $dates['start'] . self::POSTING_WINDOW_OPENS, twcfit()->tz() );
				if( $now < $challenge_starts ){
					printf ('<h3>You can&rsquo;t enter points until the challenge starts at %s on %s</h3>', $challenge_starts->format( 'g:ia' ), $challenge_starts->format( 'M j, Y' ) );
					return;
				}
			} else {
				echo "<h3>Challenge dates have not yet been set</h3>";
				return;
			}

			if( $today_window_start < $now && $now < $today_window_end ){
				printf( '<h3>Sorry, Point entry is suspended until %s</h3>', $today_window_end->format( 'g:ia' ) );
				return;
			}

		endif;

		// default - enter points for today
		$the_entry_date = $now;
		if( $now < $today_window_start ){
			// if before 3:00pm, enter points for yesterday
			$the_entry_date = $now->sub( new DateInterval('P1D') );
		}
		$entry_date = $the_entry_date->format( 'Y-m-d' );

		// but first, let's see if we already have an entry for this date
		$entry = twcfit_points()->get_points( bp_get_group_id(), get_current_user_id(), $entry_date );
		if( $entry ) {
			printf( '<h3>You have already entered points for %s</h3>', $the_entry_date->format( 'l, F jS' ) );
			return;
		}

		$free_day = twcfit_points()->get_free_day( bp_get_group_id(), get_current_user_id() );
		printf( '<h3>You are entering points for %s</h3>', $the_entry_date->format( 'l, F jS' ) );
		$this->show_form( $entry_date, $free_day );
	}

	private function show_form( $date, $free_day ){

		$nonce_key = sprintf( 'twc-points-%d-%d-%s', get_current_user_id(), bp_get_group_id(), $date );
		$points_limits = apply_filters( 'twcfit_challenge_points_max', array( 'n' => 12, 'f' => 3, 'w' => 2 ) );
?>
	<div id="record-challenge-points-result">
		<form id="record-challenge-points" method="post">
			<input type="hidden" name="userid" value="<?php echo get_current_user_id(); ?>" />
			<input type="hidden" name="groupid" value="<?php echo bp_get_group_id(); ?>" />
			<input type="hidden" name="entry-date" value="<?php echo $date; ?>" />
			<?php wp_nonce_field( $nonce_key ); ?>
			<div class="as-table">
				<div class="as-table-row">
					<div class="as-table-cell" id="add-points-error-message" ></div>
				</div>
			</div
			<div class="as-table">
				<div class="as-table-row">
					<div class="nutrition as-table-cell thirds" >
						<h3>Total Nutrition Points Earned*</h3>
						<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['n']; ?> name="points[nutrition]" id="points-nutrition" value=5 />
						<p>Please enter a value less than or equal to <?php echo $points_limits['n']; ?>.</p>
						<p>Start each day with 5 points then add or subtract based on choices you make.
						Earn a maximum of <?php echo $points_limits['n']; ?> nutrition points per day. You&rsquo;ll automatically receive a
						bonus point after posting 5 consecutive &ldquo;<?php echo $points_limits['n']; ?> point&rdquo; days!</p>
					</div>
					<div class="fitness as-table-cell thirds" >
						<h3>Total Fitness Points Earned*</h3>
						<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['f']; ?> name="points[fitness]" id="points-fitness" value="" />
						<p>Please enter a value less than or equal to <?php echo $points_limits['f']; ?>.</p>
						<p>Earn your fitness points when you exercise for at least 30 minutes. Earn a maximum
						of <?php echo $points_limits['f']; ?> fitness points per day (including the bonus points for stretching and extreme fitness!)</p>
					</div>
					<div class="lifestyle as-table-cell thirds">
						<h3>Total Wellness Points Earned*</h3>
						<input type="number" data-validation="number" min=0 max=<?php echo $points_limits['w']; ?> name="points[wellness]" id="points-wellness" value="" />
						<p>Please enter a value less than or equal to <?php echo $points_limits['w']; ?>.</p>
						<p>Score one point when you practice the lifestyle/wellness practices for the current week.
						Score a second point for posting a daily reflection.</p>
					</div>
				</div>
			</div>
			<div class="as-table">
				<div class="as-table-row">
					<div class="as-table-cell full-width">
						<div class="bonus">
							<h3>Free Nutrition Day</h3>
							<?php if( ! $free_day ) : ?>

								<input type="hidden" value="no" name="free-day" id="free-day" />
								<label for="free-day"><input type="checkbox" value="yes" name="free-day" id="free-day" />
								Count this as my Free Day</label>
								<p>If you select today as your FREE nutrition day, you will receive <?php echo $points_limits['n']; ?> points for the Nutrition Challenge today.
								Each player may take ONE free day during the Challenge. This day does not count toward the Bonus Point (5 consecutive
								days of <?php echo $points_limits['n']; ?> points for Nutrition)</p>
							<?php else : ?>
								<p>You&rsquo;ve already taken your free day for this challenge.</p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<input type="submit" name="submit" value="Submit" />
		</form>
	</div>
<?php
	}

	public function register_points(){

		if( isset( $_POST['points'] ) ){

			$args = array();
			parse_str( $_POST['points'], $args );
			$entry_date = new DateTime( $args['entry-date'], twcfit()->tz() );
			$nonce_key = sprintf( 'twc-points-%d-%d-%s', $args['userid'], $args['groupid'], $entry_date->format( 'Y-m-d' ) );

			if( wp_verify_nonce( $args['_wpnonce'], $nonce_key ) ){

				$update = array(
					'user_id'	=> (int) $args['userid'],
					'challenge_id'	=> $args['groupid'],
					'activity_date' => $entry_date->format( 'Y-m-d' ),
					'free_day'		=> ( 'no' == $args['free-day'] ) ? 0 : 1,
				);
				foreach( array( 'nutrition' => 'n', 'fitness' => 'f', 'wellness' => 'w') as $key => $point ) :
					if( isset( $args['points'][$key] ) ) $update[ $point . '_points' ] = (int) $args['points'][$key];
				endforeach;

				twcfit_points()->add_points( $update );

				wp_die( json_encode( array(
					'success' => true,
					'data' => array(
						'message' => sprintf( '<h3>You have successfully recorded your points for %s</h3>', $entry_date->format( 'F jS' ) ),
					),
				) ) );
			} else {
				wp_die( json_encode( array(
				'success' => false,
				) ) );
			}
		} else {

			wp_die(-1);
		}
	}

	public static function create_new_group( $group_name ){

		$id_group = groups_create_group( array(
			'name'			=> $group_name,
			'description'	=> "The Discussion group for your challenge",
			'creator_id'	=> 1,
			'status'		=> 'private',
		) );

		return $id_group;
  	}

}
bp_register_group_extension( 'TWCfit_Challenges_BP_Points' );
endif;
