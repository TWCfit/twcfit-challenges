<?php

if( class_exists( 'BP_Group_Extension' ) ) :
class TWCfit_Challenges_BP_Totals extends BP_Group_Extension {

	public function __construct() {

		$args = array(
			'slug' 			=> 'totals',
			'name' 			=> 'Totals',
			'visibility'	=> 'access',
			'enable_nav_item'	=> 'show_tab',
		);
		parent::init( $args );

	}

	public function display( $group_id = null ){

		$group_id = bp_get_group_id();

		// figure out our date range
		$dates = TWCfit_Challenges_BuddyPress::get_challenge_dates();

		$start = new DateTime( $dates['start'], twcfit()->tz() );
		$now = new DateTime( null, twcfit()->tz() );
		$end = min( new DateTime( $dates['end'], twcfit()->tz() ), $now );

		$interval = new DateInterval( 'P1D' );

		$members_ordered_by_display_name = $this->get_members_by_display_name( $group_id );

		if ( $members_ordered_by_display_name ) :
?>
		<div class="table-wrapper challenge-totals" style="width: 100%; height: 100%; overflow: scroll;" >
			<table class="table">
				<thead>
					<tr>
						<th><strong>Name</strong></th>
						<th class="nfwt">
							<strong>&nbsp;</strong>
							<table class="nfwt">
								<tr class="point-container">
									<td class="point-type type-n">N</td>
									<td class="point-type type-f">F</td>
									<td class="point-type type-w">W</td>
									<td class="point-type type-t">T</td>
								</tr>
							</table>
						</th>
					<?php
					foreach( new DatePeriod( $start, $interval, $end->add( $interval ) ) as $challenge_date ) :
						?>
						<th class="date nfw">
							<strong><?php echo $challenge_date->format('M j'); ?></strong>
							<table class="nfw">
								<tr class="point-container">
									<td class="point-type type-n">N</td>
									<td class="point-type type-f">F</td>
									<td class="point-type type-w">W</td>
								</tr>
							</table>
						</th>
					<?php endforeach; ?>
					</tr>
				</thead>
				<tbody>

<?php
			// do we belong to this group
			if( bp_group_is_member() ) {
				$this->member_totals_row( $group_id, get_current_user_id(), $start, $end );
			}

			foreach( $members_ordered_by_display_name as $display_name => $user_id ) :
				if( $user_id == get_current_user_id() ) continue;

				$this->member_totals_row( $group_id, $user_id, $start, $end );

			endforeach;
?>
				</tbody>
			</table>
		</div><!-- .table-wrapper -->
		<div class="mobile-only">
			To scroll down or over the list, please place or tap your finger to the right on the colorful point area and scroll down or to the left with your finger.
		</div>

<?php else: ?>

	<div id="message" class="info">
	<p>This challenge has no members.</p>
	</div>

<?php
	endif;

	}

	private function get_members_by_display_name( $group_id ){

		$memberlist = array();
		if ( bp_group_has_members( array('group_id' => $group_id, 'per_page' => 99999, 'page' => 1 ) ) ) :

			while( bp_group_members() ) : bp_group_the_member();

				$display_name = strtolower( bp_get_profile_field_data( array( 'field' => 1, 'user_id' => bp_get_member_user_id() ) ) );
				$memberlist[ $display_name ] = bp_get_member_user_id();

			endwhile;

		endif;

		ksort( $memberlist );

		return $memberlist;

	}

	private function member_totals_row( $group_id, $user_id, $start, $end ){

				$summary = $this->calculate_points_summary( $group_id, $user_id, $start, $end );

?>
				<tr>
					<th class="not-fixed" style="no-wrap;"><?php esc_html_e( twc_member_name( $user_id ) ); ?></th>
						<td>
							<table class="nfwt">
								<tr class="point-container">
									<td class="point-type type-n"><?php echo (int) $summary['nutrition']; ?></td>
									<td class="point-type type-f"><?php echo (int) $summary['fitness']; ?></td>
									<td class="point-type type-w"><?php echo (int) $summary['wellness']; ?></td>
									<td class="point-type type-t"><?php echo (int) $summary['total']; ?></td>
								</tr>
							</table>
						</td>
					</th>

					<?php echo $summary['output']; // output the full calendar ?>
				</tr>
<?php
	}

	/**
	 * Calculate member points for display
	 *
	 * Summarize the points reported for each member, incorporate any bonus points
	 */
	public function calculate_points_summary( $group_id, $user_id, $start, $end ){

		$points = twcfit_points()->get_points( $group_id, $user_id );
		$points_by_date = array();
		foreach( $points as $point_object){
			$points_by_date[ $point_object->activity_date ] = $point_object;
		}

		$points_limits = apply_filters( 'twcfit_challenge_points_max', array( 'n' => 12, 'f' => 3, 'w' => 2 ) );

		// initiate the variables
		$interval = new DateInterval( 'P1D' );
		$summary = array(
			'nutrition' => 0,
			'fitness'	=> 0,
			'wellness'	=> 0,
			'total'		=> 0,
			'output'	=> '',
		);
		$twelve_point_days = 0;

		foreach( new DatePeriod( $start, $interval, $end ) as $challenge_date ) :

			$free_day_taken = false;
			$the_points = array(
				'free_day'	=> 0,
				'nutrition'	=> 0,
				'fitness'	=> 0,
				'wellness'	=> 0,
			);

			if( isset( $points_by_date[ $challenge_date->format( 'Y-m-d') ] ) ):

				$the_points = $points_by_date[ $challenge_date->format( 'Y-m-d') ];
				// Nutrition
				if( $the_points->free_day ){

					$the_points->n_points = $points_limits['n'];
					$free_day_taken = true;
					$twelve_point_days = 0;

				} else {

					// if max-point day and not a free day, count it
					if( $points_limits['n'] != $the_points->n_points ){
						$twelve_point_days = 0;
					} else {
						$twelve_point_days++;
						if( $twelve_point_days > 4 ){
							$the_points->n_points += 1;
							$twelve_point_days = 0; // reset the counter
						}
					}
				}

				$summary['nutrition'] += $the_points->n_points;
				$summary['fitness']   += $the_points->f_points;
				$summary['wellness']  += $the_points->w_points;

				$summary['total'] += ( $the_points->n_points + $the_points->f_points + $the_points->w_points );

				$summary['output'] .= sprintf(
					'<td class="point-summary"><table class="nfw"><tr><td class="score type-n %s">%d</td><td class="score type-f">%d</td><td class="score type-w">%d</td></tr></table><!-- summary --></td>',
					( $free_day_taken ) ? 'free-day-taken' : '',
					$the_points->n_points,
					$the_points->f_points,
					$the_points->w_points
				);

			else :

				$summary['nutrition'] += 0;
				$summary['fitness']   += 0;
				$summary['wellness']  += 0;

				$summary['total'] += 0;

				$summary['output'] .= sprintf(
					'<td class="point-summary"><table class="nfw"><tr><td class="score type-n %s">%d</td><td class="score type-f">%d</td><td class="score type-w">%d</td></tr></table><!-- summary --></td>', '', 0, 0, 0 );

			endif;

		endforeach;

		return $summary;

	}


}
bp_register_group_extension( 'TWCfit_Challenges_BP_Totals' );
endif;
