<?php

class TWCfit_Challenges_BuddyPress {

	function __construct(){

		add_action( 'groups_screen_group_admin_edit_details', array( $this, 'decorate_group_admin_details' ) );
		add_filter( 'groups_custom_group_fields_editable', array( $this, 'show_markup') );
		add_action( 'groups_group_details_edited', array( $this, 'save_challenge_meta' ) );
		add_action( 'groups_created_group',  array( $this, 'save_challenge_meta' ) );
		add_action( 'bp_group_header_meta' , array( $this, 'show_meta_in_header' ) );
		add_action( 'bp_directory_groups_item', array( $this, 'show_meta_in_loop' ) );
		add_action( 'bp_before_group_header', array( $this, 'mp_protect_group' ) );

	}

  	public static function get_challenge_dates( $group_id = 0 ){

		if( ! (int) $group_id ) $group_id = bp_get_group_id();

		$dates = groups_get_groupmeta( $group_id, '_twcfit_challenge_dates' );
		if( !is_array( $dates ) ) return false;

		return $dates;
	}

  	public static function show_markup(){

  		$current = '';
  		$dates = self::get_challenge_dates();
  		if( is_array( $dates ) ) $current = json_encode( $dates );

  		$plan = self::get_mepr_plan_meta();
  		if( false === $plan ) $plan = 'nothing';

?>
		<label for="twcfit_challenge_dates">Challenge Dates</label>
		<input type="text" class="date-picker" id="twcfit_challenge_dates" name="twcfit_challenge_dates" value='<?php echo $current; ?>' />

		<label for="twcfit_challenge_membership_plan">Must Belong to this Membership plan</label>
		<?php MeprProductsHelper::get_products_dropdown( $plan, 'twcfit_challenge_membership_plan'); ?>
<?php
 	}

 	private static function get_mepr_plan_meta( $group_id = 0 ){

 		if( 0 == $group_id ) $group_id = bp_get_group_id();

  		$plan = (string) groups_get_groupmeta( $group_id, '_twcfit_challenge_mepr', true );
  		if( empty( $plan ) ) return false;

  		if( -1 == $plan ) {
  			$plan = 'anything';
  		} elseif( 0 == $plan ){
  			$plan = 'nothing';
  		} else { $plan = (int) $plan; }

  		return $plan;

 	}

  	public static function save_challenge_meta( $group_id ) {

		if( isset( $_POST ) ) :

			if ( isset( $_POST['twcfit_challenge_dates'] ) ) {
				$dates = json_decode( stripslashes ( $_POST['twcfit_challenge_dates'] ), true );
				groups_update_groupmeta( $group_id, '_twcfit_challenge_dates', $dates );
			}

			if( isset( $_POST['_mepr_product_who_can_purchase-product_id'] ) ) {
				$value = $_POST['_mepr_product_who_can_purchase-product_id'][0];
				$old_plan = self::get_mepr_plan_meta( $group_id );

				if( 'nothing' == $value ){
					$plan = 0;
					if( ! ( false === $old_plan || 'anything' == $old_plan || 'nothing' == $old_plan ) ){
						delete_post_meta( $old_plan, '_twc_challenge_bp_group' );
					}
				} elseif( 'anything' == $value ){
					$plan = -1;
					if( ! ( false === $old_plan || 'anything' == $old_plan || 'nothing' == $old_plan ) ){
						delete_post_meta( $old_plan, '_twc_challenge_bp_group' );
					}
				} else {
					$plan = (int) $value;
					// update the membership meta to tie it to this group
					update_post_meta( $plan, '_twc_challenge_bp_group', $group_id );
				}
				groups_update_groupmeta( $group_id, '_twcfit_challenge_mepr', $plan );

			}

		endif;

  	}

	public static function maybe_add_user_to_bp_group( $user_id, $product_id ){

		$bp_group = get_post_meta( $product_id, '_twc_challenge_bp_group', true );
		if( empty( $bp_group ) ) return;

		groups_join_group( $bp_group, $user_id );
	}

  	public static function show_meta_in_header() {
  	}

  	public static function show_meta_in_loop() {
  	}

  	public static function decorate_group_admin_details(){

		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style( 'jquery-ui-date-range-field', plugins_url( '../assets/js/jquery-ui.min.css', __FILE__), array(), '1.11.4' );
		wp_enqueue_style( 'jquery-ui-daterangepicker', plugins_url( '../assets/js/jquery-ui-daterangepicker/jquery.comiseo.daterangepicker.css', __FILE__ ), array(), '0.4.0' );
		wp_enqueue_script( 'moment', plugins_url( '../assets/js/moment.min.js', __FILE__ ), array(), '2.10.3' );
		wp_enqueue_script( 'jquery-ui-daterangepicker', plugins_url( '../assets/js/jquery-ui-daterangepicker/jquery.comiseo.daterangepicker.js', __FILE__ ), array( 'jquery', 'jquery-ui-core', 'jquery-ui-button', 'jquery-ui-menu', 'moment' ), '0.4.0' );

		wp_enqueue_script( 'twcfit-datepicker', plugins_url( '../assets/js/twcfit-datepicker.js', __FILE__ ), array('jquery-ui-daterangepicker') );
	}

	public static function mp_protect_group(){

		$plan = groups_get_groupmeta( bp_get_group_id(), '_twcfit_challenge_mepr' );

		if( !empty($plan) && ! current_user_can( 'mepr-active', sprintf( 'membership:%d', $plan ) ) ) {

    		$product = new MeprProduct( $plan );

    		if( ! is_null( $product->ID ) ) {

    			$message = "You are not currently a member of this challenge. Click %s to join or <a href='/login'>Log In.</a>";

    			printf( $message, MeprProductsHelper::generate_product_link_html($product, "this link" ) );
    		}

			die();
		}

	}
}
