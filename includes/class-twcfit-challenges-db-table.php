<?php

class TWCfit_Challenges_Points_Table extends OneBigIdea_DB_Table {
	private static $_this;

	const TWC_TABLE = 'twc_challenge_points';

	private function __construct(){
		global $wpdb;

		$wpdb->twcfit = $wpdb->prefix . self::TWC_TABLE;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	}

	static function get_instance(){
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// http://7php.com/how-to-code-a-singleton-design-pattern-in-php-5/
		if( !is_object(self::$_this) ) self::$_this = new self();

		return self::$_this;
	}

	public function add_points( $update ){
		global $wpdb;

		$result = $wpdb->insert(  $wpdb->twcfit, $update );

		return $result;
	}

	public function update_points( $update, $where ){
		global $wpdb;

		$result = $wpdb->update( $wpdb->twcfit, $update, $where );

		return $result;
	}

	public function delete_points( $where ){
		global $wpdb;

		$result = $wpdb->delete( $wpdb->twcfit, $where );

		return $result;
	}

	public function get_points( $challenge=0, $user_id=0, $start_date = 0, $end_date = 0, $free_days = -1 ){
		global $wpdb;

		$fields = compact( $challenge, $user_id, $start_date, $end_date );
		$where = apply_filters( 'twcfit-challenges-where', $this->where( $challenge, $user_id, $start_date, $end_date, $free_days ), $fields );
		$order_by = $this->orderby( $challenge, $user_id, $start_date, $end_date );
		$order = apply_filters( 'twc-get-points-orderby', 'ASC',  $fields );
		$sql = "SELECT * FROM {$wpdb->twcfit} WHERE {$where} ORDER BY {$order_by} {$order}";

		$result = $wpdb->get_results( $sql );

		return $result;
	}

	public function get_free_day( $challenge = 0 , $user_id = 0 ){

		$dates = TWCfit_Challenges_BuddyPress::get_challenge_dates( $challenge );

		if( false === $dates ) return false;

		$free_days = $this->get_points( $challenge, $user_id, $dates['start'], $dates['end'], $free_days = 1 );

		return (bool) $free_days;
	}

	private function where( $challenge = 0, $user_id = 0, $start_date = 0, $end_date = 0, $free_days = -1 ){

		 $where = 1;

		 if( ! ( -1 == $free_days ) ) $where .= sprintf( ' AND `free_day` = %d', $free_days );

		 if( $challenge ){
		 	$where .= sprintf( ' AND `challenge_id` = %d', $challenge );
		 }

		 if( $user_id ){
		 	$where .= sprintf( ' AND `user_id` = %d', $user_id );
		 }

		 if( $start_date ){
			$filtered_start_date = $start_date;
			if( $end_date ) {
				$filtered_end_date = $end_date;
				$where .= sprintf( " AND activity_date BETWEEN CAST( %s AS DATE ) AND CAST( %s AS DATE )",
					$filtered_start_date,
					$filtered_end_date
				);
			} else {
				$where .= sprintf( " AND `activity_date` = '%s' ", $filtered_start_date );
			}
		 }

		 return $where;
	}

	private function orderby( $challenge = 0, $user_id = 0, $start_date = 0, $end_date = 0 ){

		$fields = compact( $challenge, $user_id, $start_date, $end_date );

		return apply_filters( 'twc-get-points-orderby', '`activity_date`', $fields );
	}

	private function db_table_sql(){
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();
		return array(
			"CREATE TABLE " . $wpdb->twcfit . " (
			  ID int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  user_id int(11) NOT NULL,
			  challenge_id int(11) NOT NULL,
			  activity_date date NOT NULL,
			  n_points smallint(6) NOT NULL,
			  f_points smallint(6) NOT NULL,
			  w_points smallint(6) NOT NULL,
			  free_day tinyint(1) NOT NULL DEFAULT '0',
			  date_posted timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
			) ENGINE=InnoDB " . $charset_collate . " COMMENT='Record Member transactions';",
		);

	}

	public function db_table_create(){

		$sql = $this->db_table_sql();
		dbDelta($sql);

	}

	public function db_table_update(){
		$sql = $this->db_table_sql();
		dbDelta( $sql );

		// call upgrade functions
	}

	public function get_unique_challenges(){
		global $wpdb;

		$sql = "SELECT DISTINCT(challenge_id) FROM {$wpdb->twcfit} ORDER BY challenge_id ASC;";

		$challenges = $wpdb->get_results( $sql );

		return $challenges;
	}

	public function get_unique_users_by_challenge( $challenge_id ){
		global $wpdb;

		$sql = "SELECT DISTINCT(user_id) FROM {$wpdb->twcfit} WHERE challenge_id = %d ORDER BY user_id ASC;";

		$users = $wpdb->get_results( $wpdb->prepare( $sql, $challenge_id ) );

		return $users;
	}

}
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * DB base class - picked up from Pippin Williamson (pippinsplugins)
 *
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.1
*/
abstract class OneBigIdea_DB_Table {

	/**
	 * The name of our database table
	 *
	 * @access  public
	 * @since   2.1
	 */
	public $table_name;

	/**
	 * The version of our database table
	 *
	 * @access  public
	 * @since   2.1
	 */
	public $version;

	/**
	 * The name of the primary column
	 *
	 * @access  public
	 * @since   2.1
	 */
	public $primary_key;

	/**
	 * Get things started
	 *
	 * @access  public
	 * @since   2.1
	 */
	private function __construct() {
	}

	/**
	 * Whitelist of columns
	 *
	 * @access  public
	 * @since   2.1
	 * @return  array
	 */
	public function get_columns() {
		return array();
	}

	/**
	 * Default column values
	 *
	 * @access  public
	 * @since   2.1
	 * @return  array
	 */
	public function get_column_defaults() {
		return array();
	}

	/**
	 * Retrieve a row by the primary key
	 *
	 * @access  public
	 * @since   2.1
	 * @return  object
	 */
	public function get( $row_id ) {
		global $wpdb;
		return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $this->table_name WHERE $this->primary_key = %s LIMIT 1;", $row_id ) );
	}

	/**
	 * Retrieve a row by a specific column / value
	 *
	 * @access  public
	 * @since   2.1
	 * @return  object
	 */
	public function get_by( $column, $row_id ) {
		global $wpdb;
		$column = esc_sql( $column );
		return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $this->table_name WHERE $column = %s LIMIT 1;", $row_id ) );
	}

	/**
	 * Retrieve a specific column's value by the primary key
	 *
	 * @access  public
	 * @since   2.1
	 * @return  string
	 */
	public function get_column( $column, $row_id ) {
		global $wpdb;
		$column = esc_sql( $column );
		return $wpdb->get_var( $wpdb->prepare( "SELECT $column FROM $this->table_name WHERE $this->primary_key = %s LIMIT 1;", $row_id ) );
	}

	/**
	 * Retrieve a specific column's value by the the specified column / value
	 *
	 * @access  public
	 * @since   2.1
	 * @return  string
	 */
	public function get_column_by( $column, $column_where, $column_value ) {
		global $wpdb;
		$column_where = esc_sql( $column_where );
		$column       = esc_sql( $column );
		return $wpdb->get_var( $wpdb->prepare( "SELECT $column FROM $this->table_name WHERE $column_where = %s LIMIT 1;", $column_value ) );
	}

	/**
	 * Insert a new row
	 *
	 * @access  public
	 * @since   2.1
	 * @return  int
	 */
	public function insert( $data, $type = '' ) {
		global $wpdb;

		// Set default values
		$data = wp_parse_args( $data, $this->get_column_defaults() );

		do_action( 'edd_pre_insert_' . $type, $data );

		// Initialise column format array
		$column_formats = $this->get_columns();

		// Force fields to lower case
		$data = array_change_key_case( $data );

		// White list columns
		$data = array_intersect_key( $data, $column_formats );

		// Reorder $column_formats to match the order of columns given in $data
		$data_keys = array_keys( $data );
		$column_formats = array_merge( array_flip( $data_keys ), $column_formats );

		$wpdb->insert( $this->table_name, $data, $column_formats );

		do_action( 'edd_post_insert_' . $type, $wpdb->insert_id, $data );

		return $wpdb->insert_id;
	}

	/**
	 * Update a row
	 *
	 * @access  public
	 * @since   2.1
	 * @return  bool
	 */
	public function update( $row_id, $data = array(), $where = '' ) {

		global $wpdb;

		// Row ID must be positive integer
		$row_id = absint( $row_id );

		if( empty( $row_id ) ) {
			return false;
		}

		if( empty( $where ) ) {
			$where = $this->primary_key;
		}

		// Initialise column format array
		$column_formats = $this->get_columns();

		// Force fields to lower case
		$data = array_change_key_case( $data );

		// White list columns
		$data = array_intersect_key( $data, $column_formats );

		// Reorder $column_formats to match the order of columns given in $data
		$data_keys = array_keys( $data );
		$column_formats = array_merge( array_flip( $data_keys ), $column_formats );

		if ( false === $wpdb->update( $this->table_name, $data, array( $where => $row_id ), $column_formats ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Delete a row identified by the primary key
	 *
	 * @access  public
	 * @since   2.1
	 * @return  bool
	 */
	public function delete( $row_id = 0 ) {

		global $wpdb;

		// Row ID must be positive integer
		$row_id = absint( $row_id );

		if( empty( $row_id ) ) {
			return false;
		}

		if ( false === $wpdb->query( $wpdb->prepare( "DELETE FROM $this->table_name WHERE $this->primary_key = %d", $row_id ) ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Check if the given table exists
	 *
	 * @since  2.4
	 * @param  string $table The table name
	 * @return bool          If the table name exists
	 */
	public function table_exists( $table ) {
		global $wpdb;
		$table = sanitize_text_field( $table );

		return $wpdb->get_var( $wpdb->prepare( "SHOW TABLES LIKE '%s'", $table ) ) === $table;
	}

}
