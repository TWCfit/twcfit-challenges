<?php

class TWCfit_Challenges_Member_Management {


	public function __construct() {

		// Tell Memberpress how to actually set the IPN
		add_filter( 'mepr_paypal_std_payment_vars', function( $args, $txn ){

			$args['notify_url'] = str_replace( 'action=return', 'action=ipn', $args['return'] );

			return $args;

		}, 10, 2 );

		// Deal with purchasing a member plan
		add_action( 'mepr-txn-transition-status', function( $prev_status, $new_status, $txn ){

			if( 'complete' == $new_status ) {

				TWCfit_Challenges_BuddyPress::maybe_add_user_to_bp_group( $txn->user_id, $txn->product_id );
			}

		}, 10, 3 );
	}

}
