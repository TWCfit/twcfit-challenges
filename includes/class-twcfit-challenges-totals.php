<?php

class TWCfit_Challenges_Totals {

	function __construct(){

		add_filter( 'request', array( $this, 'check_vars') );

		add_filter('mepr-pre-run-rule-redirection', array( $this, 'redirect_endpoints' ), 10, 3);

	}

	function check_vars( $vars ){

		if( isset( $vars['points'] ) ) $vars['points'] = true;

		if( isset( $vars['totals'] ) ) $vars['totals'] = true;

		return $vars;
	}

	// runs init
	function redirect_endpoints( $result, $uri, $delim ){

		if( is_singular( 'twc-challenge' ) ) {

			$the_post = get_queried_object();
			$uri = get_permalink( $the_post );

			if( MeprRule::is_uri_locked($uri) ){
				wp_redirect( $uri );
			}

			add_filter( 'template_include', array( $this, 'load_template' ), 99 );
		}

		return $result;
	}

	function load_template( $template ){

		/**
		 *  Load the template for entering points
		 */
		if( get_query_var('points') ) {

			$new_template = locate_template( array( 'challenge_point_entry.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}

		}

		/**
		 *  Load the template for showing table of points
		 */
		if( get_query_var('totals') ) {

			$new_template = locate_template( array( 'challenge_totals.php' ) );
			if ( '' != $new_template ) {

				wp_enqueue_script( 'fixed-headers', get_stylesheet_directory_uri() . '/js/Fixed-Header-Table-master/jquery.fixedheadertable.min.js', array('jquery') );
				return $new_template ;
			}

		}

		return $template;
	}

	/**
	 * Calculate member points for display
	 *
	 * Summarize the points reported for each member, incorporate any bonus points
	 */
	public static function bbg_calculate_user_array( $user_id, $start, $end ){

		$points_limits = apply_filters( 'twcfit_challenge_points_max', array( 'n' => 12, 'f' => 3, 'w' => 2 ) );

		$days = ( strtotime( $end ) - strtotime( $start ) ) / DAY_IN_SECONDS;
		$free_day = get_user_meta( $user_id, 'free_day', true );
		if( !empty($free_day) ){
			$free_day = date( 'Y-m-d', strtotime($free_day) );
		}

		// initiate the variables
		$n = $f = $w = $t = $summary['nutrition'] = $summary['fitness'] = $summary['wellness'] = $summary['total'] = 0;
		$points_cells = [];
		while ( has_sub_field( 'points', 'user_' . $user_id ) ) {

			$points_date = get_sub_field( 'date' );
			if( $free_day == $points_date && !empty($free_day) ){
				$summary['nutrition'] += $n = $points_limits['n'];
			} else {
				$summary['nutrition'] += $n = get_sub_field( 'total_nutrition_points_earned' );
			}

			$summary['fitness'] += $f = get_sub_field( 'total_fitness_points_earned' );
			$summary['wellness'] += $w = get_sub_field( 'total_wellness_points_earned' );

			$t = $n + $f + $w;
			$summary['total'] += $t;

			$points_cells[ $points_date ] = array(
				'nutrition' => $n,
				'fitness' => $f,
				'wellness'  => $w,
			);
		}

		$init = strtotime( $start ) - DAY_IN_SECONDS;
		$output = '';
		for ( $date = 1; $date <= $days + 1; $date++ ):

			$fdate = date( 'Y-m-d', $init += DAY_IN_SECONDS );
			if ( isset( $points_cells[ $fdate ] ) ) {
				$nutrition = $points_cells[ $fdate ]['nutrition'];
				$fitness   = $points_cells[ $fdate ]['fitness'];
				$wellness  = $points_cells[ $fdate ]['wellness'];
			} else {
				$nutrition = $fitness = $wellness = '0';
			}

			$output .= sprintf( '<td><table><tr><td class="score type-n %s">%d</td><td class="score type-f">%d</td><td class="score type-w">%d</td></tr></table></td>',
			( $fdate == $free_day ) ? 'free-day-taken' : '',
			$nutrition, $fitness, $wellness);

		endfor;

		$summary['output'] = $output;

		return $summary;

	}


}
add_rewrite_endpoint( 'points', EP_PERMALINK );
add_rewrite_endpoint( 'totals', EP_PERMALINK );