<?php
/*
Plugin Name: TWCfit Challenge Manager
Plugin URI: http://geek.1bigidea.com/
Description: Manage challenges
Version: 1.0
Author: Tom Ransom
Author URI: http://1bigidea.com
Network Only: false

Licensed under The MIT License (MIT)

Copyright 2015 Tom Ransom (email: transom@1bigidea.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

class TWCfit_Challenges {
	private static $_this;
	var $plugin_slug = "TWCfit_Challenges";
	var $class_prefix = 'TWCfit_Challenges_';
	var $plugin_name = "TWCfit Challenge Manager";
	var $plugin_version = "1.0";

	const TBL_VERSION = '1.0.0';

	private function __construct() {
		global $wpdb;

		register_activation_hook(   __FILE__, array( __CLASS__, 'activate'   ) );
		register_deactivation_hook( __FILE__, array( __CLASS__, 'deactivate' ) );

		add_action('init', array($this, 'init'), 9 );

		spl_autoload_register( array($this, 'class_file_autoloader') );

	}
	function __destruct(){
        // flush_rewrite_rules();
	}
	public static function activate() {
		global $wpdb;
		// Add options, initiate cron jobs here

        flush_rewrite_rules();

			}
	function deactivate() {
		// Remove cron jobs here
	}
	function uninstall() {
		// Delete options here
	}
	static function get_instance(){
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// http://7php.com/how-to-code-a-singleton-design-pattern-in-php-5/
		if( !is_object(self::$_this) ) self::$_this = new self();

		return self::$_this;
	}

	/**
	 * Autoloads files when requested
	 *
	 * @since  1.0.0
	 * @param  string $class_name Name of the class being requested
	 * http://dsgnwrks.pro/how-to/using-class-autoloaders-in-wordpress/
	 */
	function class_file_autoloader( $class_name ) {

		/**
		 * If the class being requested does not start with our prefix,
		 * we know it's not one in our project
		 */
		if ( 0 !== strpos( $class_name, $this->class_prefix ) ) {
			return;
		}

		$file_name = str_replace(
			array( $this->class_prefix, '_' ),      // Prefix | Underscores
			array( '', '-' ),         // Remove | Replace with hyphens
			strtolower( $class_name ) // lowercase
		);

		// Compile our path from the current location
		$file = dirname( __FILE__ ) . '/includes/class-'. $file_name .'.php';

		// If a file is found
		if ( file_exists( $file ) ) {
			// Then load it up!
			require_once( $file );
		}
	}

	/**
	 *	Functions below actually do the work
	 */
	function init(){
		global $wpdb;

		require_once( 'includes/class-twcfit-challenges-db-table.php' );

		// create the TWCFit Points database table
		if( ! twcfit_points()->table_exists( $wpdb->prefix . TWCfit_Challenges_Points_Table::TWC_TABLE ) )
		{
			twcfit_points()->db_table_create();

			update_option( 'twcfit_db_version', self::TBL_VERSION );

		} else {

			$ver = get_option( 'twcfit_db_version', '0.0.0');

			if( version_compare ( self::TBL_VERSION, $ver, '>' ) ){
				twcfit_points()->db_table_update();
				update_option( 'twcfit_db_version', self::TBL_VERSION );
			}
		}

		$timezone = get_option( 'timezone_string' );

		if( empty( $timezone ) && class_exists( 'Helper_DateTimeZone' ) ) {

			$gmt_offset = get_option( 'gmt_offset' );
			$this->tz = new Helper_DateTimeZone( Helper_DateTimeZone::tzOffsetToName( $gmt_offset ) );

		} else {

			$this->tz = new DateTimeZone( $timezone );

		}

		new TWCfit_Challenges_Member_Management();

		new TWCfit_Challenges_BuddyPress();
	}

	public function tz(){
		return $this->tz;
	}

}
function twcfit(){
	return TWCfit_Challenges::get_instance();
}
twcfit();
function twcfit_points(){
	return TWCfit_Challenges_Points_Table::get_instance();
}

add_action( 'admin_menu', function(){
	new TWCfit_Challenges_Admin_Menus();
} );
add_action( 'admin_enqueue_scripts', function(){
	new TWCfit_Challenges_Administration();
} );
add_action( 'wp_ajax_twcfit-points-editor', function(){

	new TWCfit_Challenges_Admin_Points();

} );

function twc_member_name( $user_id = 0 ){

	if( 0 == $user_id ) $user_id = get_current_user_id();

      $userdata = get_userdata( $user_id );
      $usermeta = get_user_meta( $user_id );

	return bp_get_profile_field_data( array( 'field' => 1, 'user_id' => $user_id ) );

}



register_activation_hook(   __FILE__, array( 'TWCfit_Challenges', 'activate'   ) );
register_deactivation_hook( __FILE__, array( 'TWCfit_Challenges', 'deactivate' ) );
register_uninstall_hook(    __FILE__, array( 'TWCfit_Challenges', 'uninstall' ) );

if( ! class_exists( 'Helper_DateTimeZone' ) ) {
/**
* Helps with timezones.
* @link http://us.php.net/manual/en/class.datetimezone.php
*
* @package  Date
*/
class Helper_DateTimeZone extends DateTimeZone
{
    /**
     * Converts a timezone hourly offset to its timezone's name.
     * @example $offset = -5, $isDst = 0 <=> return value = 'America/New_York'
     *
     * @param float $offset The timezone's offset in hours.
     *                      Lowest value: -12 (Pacific/Kwajalein)
     *                      Highest value: 14 (Pacific/Kiritimati)
     * @param bool  $isDst  Is the offset for the timezone when it's in daylight
     *                      savings time?
     *
     * @return string The name of the timezone: 'Asia/Tokyo', 'Europe/Paris', ...
     */
    final public static function tzOffsetToName($offset, $isDst = null)
    {
        if ($isDst === null)
        {
            $isDst = date('I');
        }

        $offset *= 3600;
        $zone    = timezone_name_from_abbr('', $offset, $isDst);

        if ($zone === false)
        {
            foreach (timezone_abbreviations_list() as $abbr)
            {
                foreach ($abbr as $city)
                {
                    if ((bool)$city['dst'] === (bool)$isDst &&
                        strlen($city['timezone_id']) > 0    &&
                        $city['offset'] == $offset)
                    {
                        $zone = $city['timezone_id'];
                        break;
                    }
                }

                if ($zone !== false)
                {
                    break;
                }
            }
        }

        return $zone;
    }
}
}

add_action( 'bp_include', function(){

	require_once( 'includes/class-twcfit-challenges-bp-points.php' );
	require_once( 'includes/class-twcfit-challenges-bp-totals.php' );

	add_action( 'bp_enqueue_scripts', array( 'TWCfit_Challenges_BP_Points', 'enqueue_scripts' ) );

} );
